.. _RISC-V_CPU_core_in_FPGA_fabric:

Low-latency I/O RISC-V CPU core in FPGA fabric - Atharva Kashalkar
##################################################################

Introduction
*************

Implementation of PRU subsystem on BeagleV-Fire's FPGA fabric, resulting in a real-time microcontroller system working alongside the main CPU, providing low-latency access to I/O .

Summary links
=============

- **Contributor:** `Atharva Kashalkar <https://forum.beagleboard.org/u/roger18>`_
- **Mentors:** `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_, `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_
- **Code Repository:** TBD 

Status
=======

This project is currently just a proposal.

Proposal
========

| Accounts created across `OpenBeagle <https://openbeagle.org/Roger18>`_, `Discord <https://discordapp.com/users/539789356228018200>`_ and `Beagle Forum <https://forum.beagleboard.org/u/roger18>`_
| PR was created for the cross-compilation task.

About 
=====

- **Resume:** Find my resume `here <https://docs.google.com/document/d/1NK4dAuWjMRkkG4YJrMSHrUTxJs0s7_JUR-DipmuclWQ/edit?usp=sharing>`_
- **Forum:** :fab:`discourse` `u/roger18 (Atharva Kashalkar) <https://forum.beagleboard.org/u/roger18>`_
- **OpenBeagle:** :fab:`gitlab` `Roger18 (Atharva Kashalkar) <https://openbeagle.org/Roger18>`_
- **Github:** :fab:`github` `RapidRoger18 (Atharva Kashalkar) <https://github.com/RapidRoger18>`_
- **School:** :fas:`school` Veermata Jijabai Technological Institute (VJTI)
- **Country:** :fas:`flag` India
- **Primary language:** :fas:`language` English Hindi
- **Typical work hours:** :fas:`clock` 9 AM-11 PM Indian Standard Time
- **Previous GSoC participation:** :fab:`google` First Time Applicant

Project
********

**Project name:** Low-latency I/O RISC-V CPU core in FPGA fabric.

Description
============
To provide the capability of a Programmable Real-time Unit Industrial Control SubSystem (PRU-ICSS), which is present on several BeagleBone boards, I propose to deploy an existing RISC-V 32IM core with a customized Instruction Set Architecture on FPGA Fabric present on BeagleV-Fire. The goal of this deployment is to provide high bandwidth between the CPU and I/O, resulting in a on-board microcontroller.

Goals and Objectives
====================
The ultimate aim of this project is to have a functional Risc-V soft core on the BeagleV-Fire FPGA fabric, which will be functionally equivalent to a PRU subsystem on the BeagleBone Black. Together with ultra-low latency I/O operations, this core will be able to execute Risc-V instructions acting as a microcontroller. BeagleV-Fire will feature a functional PRU-comparable subsystem on its FPGA fabric by the project's conclusion.

The programmable nature of the PRU, along with its access to pins, events, and all SoC resources, provides flexibility in implementing fast real-time responses, specialized data handling operations, custom peripheral interfaces, and in offloading tasks from the other processor cores of the system-on-chip (SoC).
This is a basic block diagram for PRU design: 

.. image:: ../_static/images/PRU_diagram.png 

Based on the block diagram above we can divide the project into two stages.
Stage 1 will include deciding the most suitable pre-existing core based on its performance concerning its size, complexity, accessibility, etc.
Customizing the core to meet project requirements and integrating it with BeagleV-Fire gateware, followed by extensive Verification of the modifications using preset verification techniques.  
This stage will conclude with having a stable communication protocol between the PRU and main CPU. This will ensure that the PRU's instruction memory can be written by main CPU and have an interrupt signal to control the PRU.

At Stage 1 Evaluation, a functional Risc-V PRU with CPU access support will be prepared for deployment.  

The primary aim for Stage 2 will be establishing the necessary I/O functions, and stable communication between the PRU and these I/O.
I/O and other peripherals can be mapped to particular special addresses to which the PRU can write, which will enable the user to configure the I/O devices to any parameters using a PRU C Library, which will try to abstract I/O usage for the user.
Changes to be made to build scripts to add an option of including a PRU design to the gateware when flashing the Board. Updating Device Tree Overlay to enable PRU whenever necessary. Having a test program for users to get familiar with the use of PRU.

After Stage 2, BeagleV-Fire will host a fully functional PRU system that can be controlled by invoking a specific function within the main CPU.

Software
=========

 - Verilog HDL.
 - Verilator.
 - Libero SoC suite.
 - Microchip Softconsole
 - ModelSim ME.
 - Linux.
 - OpenBeagle CI.
 
Hardware
========

Ability to program BeagleV-Fire using serial port and set up JTAG for effective debugging. 

Timeline
********

Timeline summary
=================

.. table:: 

    +------------------------+----------------------------------------------------------------------------------------------------+
    | Date                   | Activity                                                                                           |                                  
    +========================+====================================================================================================+
    | April                  | Understand detailed use cases of existing cores and shortlist them based on requirements           |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 1                  | Start bonding - Discussing implementation methods with Mentors                                     |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 15                 | College Examinations                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 1                 | Start coding and introductory video                                                                |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 3                 | :ref:`RVFMilestone1`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 10                | :ref:`RVFMilestone2`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 17                | :ref:`RVFMilestone3`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 24                | :ref:`RVFMilestone4`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 1                 | :ref:`RVFMilestone5`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 8                 | Submit midterm evaluations                                                                         |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 15                | :ref:`RVFMilestone6`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 22                | :ref:`RVFMilestone7`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 29                | :ref:`RVFMilestone8`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 5               | :ref:`RVFMilestone9`                                                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 12              | :ref:`RVFMilestone10`                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 19              | Submit final project video, submit final work to GSoC site, and complete final mentor evaluation   |
    +------------------------+----------------------------------------------------------------------------------------------------+

Timeline detailed
=================

Community Bonding Period (May 1st - May 15th)
-------------------------------------------------------

 - Get to know mentors and discuss project implementation.
 - read documentation, and get up to speed to begin working on the projects
 - shortlisting pre-existing cores based on initial assessment, by reading available documentation. 

Coding begins (May 27th)
-------------------------------------------------------

.. _RVFMilestone1:
Milestone #1, Introductory YouTube video (June 3rd)
-------------------------------------------------------
 - Make an introductory video
 - Selecting the best-suited core by comparing their functionality, size, availability of extensions, etc.
 - Setting up remote access on BeagleV-Fire and completing the LED-blink tutorial given in the documentation.

.. _RVFMilestone2:
Milestone #2, Modifiying RV core (June 10th)
-------------------------------------------------------
 - Modification of the selected core to meet project requirements, like memory configurations, interrupt control, etc.
 - Removing unnecessary extensions to reduce size and complexity, without changing its efficiency.

.. _RVFMilestone3:
Milestone #3, Verification of core (June 17th)
-------------------------------------------------------
 - Performing Verification of PRU core using pre-determined verification methods by using Verilator or any other Verification software.
 - This is to make sure the PRU functions as required after modifications.  

.. _RVFMilestone4:
Milestone #4, Remote-proc setup (June 24th)
-------------------------------------------------------
 - Integration of PRU core with BeagleV-Fire gateware. This is to ensure PRU deployment through BeagleV-Fire gateware.
 - Set up Remote-proc Interfacing between PRU and main CPU. This will ensure CPU access to PRU's instruction memory.

.. _RVFMilestone5:
Milestone #5, Setup Device Tree (July 1st)
-------------------------------------------------------
 - Coninuing to work on Remote-proc framework.
 - Ensuring stable PRU workflow. 
 - Setting up Device Tree Overlay to include PRU when necessary.

Submit midterm evaluations (July 8th)
-------------------------------------------------------
 - Complete pending Stage 1 tasks, if any.

.. important:: 
    
    **July 12 - 18:00 UTC:** Midterm evaluation deadline (standard coding period) 

.. _RVFMilestone6:
Milestone #6, Mapping I/O (July 15th)
-------------------------------------------------------
 - Implementing required I/O functionalities.
 - Mapping I/O to registers to enable interaction with CPU. 

.. _RVFMilestone7:
Milestone #7, Verification of mapped I/O (July 22nd)
-------------------------------------------------------
 - Verification of these I/O operations using simulation and generating common use cases.
 - Deploying these modules on BeagleV-Fire FPGA and testing their latency and real-time application. 

.. _RVFMilestone8:
Milestone #8, Add customizability (July 29th)
-------------------------------------------------------
 - Adding customizability to CPU. This is to allow user to make small changes to CPU to observe the changes in output.
 - This will allow user to learn internal working of a RISC processor. 

.. _RVFMilestone9:
Milestone #9, Setup Scripts (Aug 5th)
-------------------------------------------------------
 - Setting up gateware scripts wherever changes are needed. This will grant users easier access to CPU.
 - Editing TCL scripts wherever necessary.

.. _RVFMilestone10:
Milestone #10, Documentation and Tutorial(Aug 12th)
-------------------------------------------------------
 - Documenting the project and ways to access PRU on docs.beagleboard.org.
 - Having an LED Blink tutorial for users to familiarize themselves with the PRU.

Final YouTube video (Aug 19th)
-------------------------------------------------------

Submit final project video, submit final work to GSoC site, and complete final mentor evaluation

Final Submission (Aug 24th)
-------------------------------------------------------

.. important::

    **August 19 - 26 - 18:00 UTC:** Final week: GSoC contributors submit their final work 
    product and their final mentor evaluation (standard coding period)

    **August 26 - September 2 - 18:00 UTC:** Mentors submit final GSoC contributor 
    evaluations (standard coding period)

Initial results (September 3)
-------------------------------------------------------

.. important:: 
    **September 3 - November 4:** GSoC contributors with extended timelines continue coding

    **November 4 - 18:00 UTC:** Final date for all GSoC contributors to submit their final work product and final evaluation

    **November 11 - 18:00 UTC:** Final date for mentors to submit evaluations for GSoC contributor projects with an extended deadline

Experience
***********

This project will require prior knowledge of Risc-V ISA, FPGA programming, Assembly, and Verilog.

| I have previously worked on a project `Risc-V 32IM core <https://github.com/NachtSpyder04/RISC-V-Eklavya-23>`_ where I contributed towards the synthesis of a Risc-V core using Vivado Design suite and flashed it on UPduino FPGA using Yosys framework. This CPU was successfully able to run operations like the Fibonacci series and factorial of a number.
| I was a Team Leader of the Semi-finalist Team in `E-Yantra Robotics Competition (eYRC) <https://portal.e-yantra.org/>`_, IIT Bombay on Theme "AstroTinker Bot", where we had to develop a single cycle Risc-V 32I core that was capable of implementing Dijktra Algorithm written in C code and cross-compiled to Risc-V binary instructions. This Competition exposed me to various debugging methods using JTAG connection through Quartus Prime Lite software. DE0-NANO development board with Cyclone IV FPGA was used to control the bot. The bot used PID-based Line Following and Electromagnet to pick and place blocks through an arena representing a Space Station. Verilog HDL codes for the project can be found `here <https://github.com/RapidRoger18/E-yantra_Astrotinker-bot>`_
| I have prior experience with ESP32 microcontrollers for small projects.

Approach
*********
| As a starting point, I have forked the BeagleV-Fire gateware Repository and completed the `Blinky Tutorial <https://openbeagle.org/Roger18/gateware/-/tree/main/sources/FPGA-design/script_support/components/CAPE/BLINKY_TRY>`_ as specified on `BeagleBoard Docs <https://docs.beagleboard.org/latest/boards/beaglev/fire/demos-and-tutorials/gateware/customize-cape-gateware-verilog.html>`_ 
| Continuing on this I have set up Libero SoC suite and Softconsole on my Local machine that will provide an easy debugging method using JTAG. I have tested this setup by implementing a simple UART receiver and simulating it in ModelSim ME using a simple testbench.

| The concept of having a microprocessor and a microcontroller on a single SoC is amazing. I can see a variety of applications where a SoC like BeagleV-Fire would be ideal. This inspires me to take on this project and give my contribution to the open-source community. I have my full commitment to this project and am willing to put in complete efforts to complete the project within the allocated time.
| I would also like to keep improving with the project after GSoC.

Contingency
===========

If I get stuck on my project and my mentor isn’t around, I will use the following resources:-
 - `BeagleV-Fire <https://docs.beagleboard.io/latest/boards/beaglev/fire/index.html>`_
 - `PRU-Documentation <https://inst.eecs.berkeley.edu/~ee192/sp20/files/am335x-pru.pdf>`_
 - `Picorv32 Docs <https://github.com/YosysHQ/picorv32/blob/master/README.md>`_
 - `VexRiscv Docs <https://github.com/SpinalHDL/VexRiscv/blob/master/README.md>`_ 
 - `PRU cookbook <https://docs.beagleboard.org/books/pru-cookbook/index.html>`_
 - `TI PRU documentation <https://software-dl.ti.com/processor-sdk-linux/esd/AM62X/09_00_00_03/exports/docs/linux/Foundational_Components_PRU_Subsystem.html>`_
 
Moreover, the BeagleBoard community is very helpful in resolving doubts, I will use OpenBeagle forums to clarify any doubts left after referring to the above resources.

Benefit
========
This project will not only improve the use cases of BeagleV-Fire but also provide a very fast real-time subsystem that can be used as a microcontroller. It will provide the following functionalities: -
 - Reduce memory and I/O resource consumption on the main CPU by performing basic computations in PRU itself thus providing more resources to perform complex tasks.
 - Provide a real-time interface for fast, deterministic operations.
 - Reprogammability of FPGA will allow to deployment of PRU only when necessary, thus providing additional logical elements when PRU is not being used.
 - The configurable nature of FPGA will allow multiple levels of customizations and configurations on PRU, enabling the user to efficiently meet their requirements in the lowest possible resources.   

The success of this project will result in an all-in-one SoC meeting all the requirements of `BeagleBoard.org <https://www.beagleboard.org/>`_ community.

Misc
====

PR request for cross-compilation task `#182 <https://github.com/jadonk/gsoc-application/pull/182>`_ 
