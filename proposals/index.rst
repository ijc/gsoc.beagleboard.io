.. _gsoc-proposals:

Proposals
#########

.. tip:: 

    Checkout :ref:`gsoc-project-ideas` page to explore ideas and :ref:`gsoc-proposal-guide` page to write your own proposal.

.. toctree:: 
    :maxdepth: 1

    template
    melta101
    suraj-sonawane
    upstream_zephyr_bbai64_r5
    alecdenny
    matt-davison
    ijc
    mc
    himanshuk
    drone_cape_for_beagle-v-fire
    commercial_detection_and_replacement
    RISC-V_CPU_core_in_FPGA_fabric
